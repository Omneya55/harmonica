Feature: Register new user and login from the mobile side

Scenario: Signup using Facebook
    Given open application
    And Select Facebook signup
    #And continue as "Omneya"
    When accept the rules
    And insert in "First Name" "Omneya"
    And insert in "Last Name" "Moustafa"
    And click "Continue"
    And  insert in "DD" "05"
    And  insert in "MM" "11"
    And  insert in "YYYY" "1990"
    And click "Continue"
    #Then validate on the age
    And click "Woman"
    And select the religous beliefs
    And select the ethnicity
    And chooses where do you live
    And chooses where did you grow up
    And select relationship status
    And select kids status
    And select body type
    And select height
    And select smoke status
    And select level of education
    And select university
    And select field of study
    And select career
    And view the potential matches
    And continue
    And Choose topic
    And update the topic
    And continue
    And add a photo
    And complete the profile
    Then I should be logged in
    And I should be redirected to the discover page
    And I should be able to view some cards
