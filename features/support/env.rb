require 'capybara'
require 'capybara/dsl'
require 'capybara/cucumber'
require 'capybara/rspec'
require 'cucumber'
require 'selenium/webdriver'
require 'mysql2'
require 'appium_lib'
require 'net/http'
require 'rspec'
require 'httparty'

######################
## Main directories ##
######################

$root_dir = Dir.pwd
$app_dir = "#{$root_dir}/features/support/apps"


################
# Domain Setup #
################
if ENV['HARMONICA_DOMAIN']
  case ENV['HARMONICA_DOMAIN'].downcase
    when nil
      ENV['HARMONICA_DOMAIN'] = 'test'
  end
else
  ENV['HARMONICA_DOMAIN'] = 'test'
end

##############
# APIs Setup #
##############
version = 'v1'
$apis_base = "http://api.harmonica-#{ENV['HARMONIKA_DOMAIN']}.app/dating/#{version}"


#############
#Appium #


def start_app(app)
common_caps = {
  deviceName: "Anyname"+rand(1..100).to_s,
  platformName: "Android",
  autoGrantPermissions: true, # work for Android
  autoAcceptAlerts: true, # work only for iOS
  newCommandTimeout: 3600,
  appPackage: "com.harmonika",
  appActivity: "com.harmonika.MainActivity",
  app: (File.join(File.dirname(__FILE__), "apps/#{app}.apk")),
  adbPort: 5037,
  dontStopAppOnReset: true
}

opts = {caps: common_caps}

  begin
      $harmonica = Appium::Driver.new(opts, true)
      $harmonica = Appium.promote_appium_methods Object
      $appium = $harmonica.start_driver

  rescue
    raise
  end
  $appium.driver.manage.timeouts.implicit_wait = 40
  @Active_app = app
end
